package com.usyd.elec5619.group14;

import com.usyd.elec5619.group14.model.Application;
import com.usyd.elec5619.group14.model.Comment;
import com.usyd.elec5619.group14.model.Employer;
import com.usyd.elec5619.group14.model.JobPost;
import com.usyd.elec5619.group14.model.JobSeeker;
import com.usyd.elec5619.group14.model.Role;
import com.usyd.elec5619.group14.model.User;
import com.usyd.elec5619.group14.model.constants.Industry;
import com.usyd.elec5619.group14.model.constants.JobType;
import com.usyd.elec5619.group14.repository.ApplicationRepository;
import com.usyd.elec5619.group14.repository.CommentRepository;
import com.usyd.elec5619.group14.repository.EmployerRepository;
import com.usyd.elec5619.group14.repository.JobPostRepository;
import com.usyd.elec5619.group14.repository.JobSeekerRepository;
import com.usyd.elec5619.group14.repository.RoleRepository;
import com.usyd.elec5619.group14.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;
import java.util.Arrays;

@Component
public class Seeder {
    private static final SecureRandom random = new SecureRandom();

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private JobSeekerRepository seekerRepository;
    @Autowired
    private EmployerRepository employerRepository;
    @Autowired
    private JobPostRepository jobPostRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ApplicationRepository applicationRepository;
    
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @EventListener
	public void seed(ContextRefreshedEvent event) {
		Role role = new Role(1L, "USER_EMPLOYER");		
		Role role2 = new Role(2L, "USER_JOBSEEKER");
		

		roleRepository.save(role);
        roleRepository.save(role2);
        
        seedUsers();
        seedJobPosts();
        seedComments();
        seedApplications();
    }

    private void seedUsers() {
        for(int i = 1; i <= 10; i++) {
            if(i % 2 == 0) {
                JobSeeker seeker = new JobSeeker();

                seeker.setUserId(i);
                seeker.setFirstName("FirstName" + i);
                seeker.setLastName("LastName" + i);
                seeker.setEmail("dummy.email" + i + "@dummy.lol");
                seeker.setAddress("address" + i);
                seeker.setPhoneNumber("0123123" + i);
                seeker.setPassword(this.passwordEncoder.encode("123" + i));

                seeker.setJobType(randomEnum(JobType.class));
                seeker.setQualification("qualifications " + i);
                seeker.setSkills("skills " + i);
                seeker.setWorkExperience("workExperience " + i);

                seeker.setRoles(Arrays.asList(roleRepository.findById((long) 2).get()));
                
                seeker.setBirthDate("1991-01-01");
                

                seekerRepository.save(seeker);
            } else {
                Employer employer = new Employer();

                employer.setUserId(i);
                employer.setFirstName("FirstName" + i);
                employer.setLastName("LastName" + i);
                employer.setEmail("dummy.email" + i + "@dummy.lol");
                employer.setAddress("address" + i);
                employer.setPhoneNumber("0123123" + i);
                employer.setPassword(this.passwordEncoder.encode("123" + i));

                employer.setCompanyDescription("companyDescription " + i);
                employer.setCompanyName("companyName " + i);
                employer.setIndustry(randomEnum(Industry.class));
                
                employer.setRoles(Arrays.asList(roleRepository.findById((long) 1).get()));

                employerRepository.save(employer);
            }
        }
    }

    private void seedJobPosts() {
        JobPost jobPost = new JobPost();
        Employer employer = employerRepository.findById((long)1).get();

        jobPost.setJobPostId((long)1);
        jobPost.setEmployer(employer);
        jobPost.setJobPostApplicationDeadline("2018-10-29T23:59");
        jobPost.setJobPostDesc("description 1");
        jobPost.setJobPostIndustry(employer.getIndustry());
        jobPost.setJobPostPosition("position 1");
        jobPost.setJobPostLocation("location 1");
        jobPost.setJobPostPostedTime();
        jobPost.setJobPostSalary(1000.0);
        jobPost.setJobPostTitle("title 1");
        jobPost.setJobPostType(randomEnum(JobType.class));

        jobPostRepository.save(jobPost);

        jobPost = new JobPost();

        jobPost.setJobPostId((long)1);
        jobPost.setEmployer(employer);
        jobPost.setJobPostApplicationDeadline("2018-10-29T23:59");
        jobPost.setJobPostDesc("description 2");
        jobPost.setJobPostIndustry(employer.getIndustry());
        jobPost.setJobPostPosition("position 2");
        jobPost.setJobPostLocation("location 2");
        jobPost.setJobPostPostedTime();
        jobPost.setJobPostSalary(1000.0);
        jobPost.setJobPostTitle("title 2");
        jobPost.setJobPostType(randomEnum(JobType.class));

        jobPostRepository.save(jobPost);
    }
    
    private void seedComments() {
    	Comment comment = new Comment();
    	JobPost jobPost = jobPostRepository.findById(11L).get();
    	User user = this.userRepository.findById(10L).get();
    	comment.setCommentId((long)1);
    	comment.setCommentBody("comment body 1");
    	comment.setCommentTime();
    	comment.setJobPost(jobPost);
    	comment.setUser(user);
    	commentRepository.save(comment);
    }

    private void seedApplications() {
        Application application = new Application();
        JobPost jobPost = jobPostRepository.findById(11L).get();
        JobSeeker seeker = seekerRepository.findById(2L).get();

        application.setApplicationStatus(false);
        application.setJobPost(jobPost);
        application.setJobSeeker(seeker);

        applicationRepository.save(application);

        application = new Application();
        jobPost = jobPostRepository.findById(12L).get();
        seeker = seekerRepository.findById(2L).get();

        application.setApplicationStatus(false);
        application.setJobPost(jobPost);
        application.setJobSeeker(seeker);

        applicationRepository.save(application);
    }

    private static <T extends Enum<?>> T randomEnum(Class<T> clazz){
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

}