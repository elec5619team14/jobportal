package com.usyd.elec5619.group14;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jobportal14Application {

	public static void main(String[] args) {
		SpringApplication.run(Jobportal14Application.class, args);
	}
}
