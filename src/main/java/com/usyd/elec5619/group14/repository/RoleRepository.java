package com.usyd.elec5619.group14.repository;

import org.springframework.data.repository.CrudRepository;

import com.usyd.elec5619.group14.model.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
	
}
