package com.usyd.elec5619.group14.repository;

import org.springframework.data.repository.CrudRepository;

import com.usyd.elec5619.group14.model.Employer;


public interface EmployerRepository extends CrudRepository<Employer, Long>{
}
