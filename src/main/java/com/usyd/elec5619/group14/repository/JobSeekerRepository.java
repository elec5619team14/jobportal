package com.usyd.elec5619.group14.repository;

import org.springframework.data.repository.CrudRepository;

import com.usyd.elec5619.group14.model.JobSeeker;


public interface JobSeekerRepository extends CrudRepository<JobSeeker, Long> {


}
