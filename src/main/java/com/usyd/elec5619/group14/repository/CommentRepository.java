package com.usyd.elec5619.group14.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

import com.usyd.elec5619.group14.model.Comment;


public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findByJobPostJobPostId(Long id);
}
