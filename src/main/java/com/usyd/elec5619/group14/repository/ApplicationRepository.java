package com.usyd.elec5619.group14.repository;


import org.springframework.data.repository.CrudRepository;

import com.usyd.elec5619.group14.model.Application;


public interface ApplicationRepository extends CrudRepository<Application, Long> {

}
