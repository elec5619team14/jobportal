package com.usyd.elec5619.group14.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.usyd.elec5619.group14.model.JobPost;

@Repository
public interface JobPostRepository extends JpaRepository<JobPost,Long> {
	//List<JobPost> getJobPostByEmployer(User user);
}
