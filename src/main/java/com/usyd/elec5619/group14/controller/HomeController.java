package com.usyd.elec5619.group14.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;


import com.usyd.elec5619.group14.model.User;
import com.usyd.elec5619.group14.repository.UserRepository;
import com.usyd.elec5619.group14.service.ApplicationService;
import com.usyd.elec5619.group14.service.CommentService;
import com.usyd.elec5619.group14.service.EmployerService;
import com.usyd.elec5619.group14.service.JobPostService;
import com.usyd.elec5619.group14.service.JobSeekerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

@Controller
@RequestMapping("/jobportal")
public class HomeController {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private JobPostService jobPostService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private ApplicationService applicationService;
	@Autowired
	private EmployerService employerService;
	@Autowired
	private JobSeekerService jobSeekerService;
	
	@GetMapping
	public String homePage(Model model, HttpSession session) {
		
		//show user's email on dashboard
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = userRepository.findByEmail(userDetails.getUsername());

		session.setAttribute("userIdSession", user.getUserId());
		session.setAttribute("userRole", user.getRoles().get(0).getName());
		
		model.addAttribute("useremail", userDetails.getUsername());
		
		model.addAttribute("users",this.userRepository.findAll().size());
		model.addAttribute("jobposts",String.valueOf(this.jobPostService.listJobPosts().size()));
		model.addAttribute("recentjobs",this.jobPostService.listJobPosts());
		model.addAttribute("recentcomments", this.commentService.listComments());
		model.addAttribute("employers", this.employerService.listEmployers().size());
		model.addAttribute("jobseekers",this.jobSeekerService.listJobSeekers().size());
		model.addAttribute("comments",this.commentService.listComments().size());
		model.addAttribute("applications",this.applicationService.listApplications().size());
		return "dashboard";
	}
	@GetMapping("/aboutus")
	public String aboutUsPage(Model model) {
		return "aboutus";
	}
	@GetMapping("/contactus")
	public String contactUsPage(Model model) {
		return "contactus";
	}
	
	@GetMapping("/login")
	public String login(Model model) {
		return "login";
	}
	
	@GetMapping("/user")
    public String userIndex() {
        return "user/dashboard";
    }
	
}
