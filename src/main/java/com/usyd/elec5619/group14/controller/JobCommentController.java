package com.usyd.elec5619.group14.controller;

import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.usyd.elec5619.group14.model.Comment;
import com.usyd.elec5619.group14.model.User;
import com.usyd.elec5619.group14.repository.UserRepository;
import com.usyd.elec5619.group14.service.CommentService;
import com.usyd.elec5619.group14.service.JobPostService;
import com.usyd.elec5619.group14.service.UserService;

@Controller
@RequestMapping("/jobportal/comment")
public class JobCommentController {
	
	@Autowired
	private CommentService commentService;
	@Autowired
	private JobPostService jobPostService;
	@Autowired 
	private UserService userService;
	
	@RequestMapping("")
	public String getComment(Model model, HttpSession session) {
		if(session.getAttribute("userIdSession") == null) {
			return "redirect:/jobportal";
		}
		model.addAttribute("comment", new Comment());
		model.addAttribute("comments", this.commentService.listComments());
		return "comment";
	}
/*
	@RequestMapping("/add")
	public String commentform(Model model) {
		model.addAttribute("comment", new Comment());
		return "commentform";
	}
*/
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addJobComment(@RequestParam("jobPostId") long jid, @ModelAttribute Comment comment, Model model) {
		comment.setJobPost(this.jobPostService.getJobPost(jid));
		UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = userService.findByEmail(userDetails.getUsername());
		comment.setUser(user);
		comment.setParentId(0);
		comment.setCommentTime();
		this.commentService.addComment(comment);
		return "redirect:/jobportal/jobpost/view/"+jid;
	}

	@RequestMapping(value = "/edit/{jobPostId}/{commentId}")
	public String editCommentForm(@PathVariable("jobPostId") long jid, @PathVariable("commentId") long cid, Model model) {
		model.addAttribute("comment", this.commentService.getCommentById(cid));
		model.addAttribute("jid",jid);
		return "commentform";
	}

/*	@RequestMapping(value = "/view/{jobPostId}/{commentId}")
	public String viewComment(@PathVariable("jobPostId") long jobPostId, @PathVariable("commentId") long commentId, Model model) {
		model.addAttribute("comment", this.commentService.getCommentById(commentId));
		model.addAttribute("jobPost",this.jobPostService.getJobPost(jobPostId));
		return "viewjobpost";
	}
*/
	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	public String updateComment(@RequestParam("jobPostId") Long jid, @ModelAttribute Comment comment, Model model) {
		commentService.updateComment(comment);
		return "redirect:/jobportal/jobpost/view/"+jid;
	}

	@RequestMapping("/delete/{jobPostId}/{commentId}")
	public String deleteComment(@PathVariable("jobPostId") long jid,@PathVariable("commentId") long cid) {
		commentService.removeComment(cid);
		return "redirect:/jobportal/jobpost/view/"+jid;
	}
}
