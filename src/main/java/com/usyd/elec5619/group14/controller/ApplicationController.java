package com.usyd.elec5619.group14.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.usyd.elec5619.group14.model.Application;
import com.usyd.elec5619.group14.model.JobPost;
import com.usyd.elec5619.group14.model.JobSeeker;
import com.usyd.elec5619.group14.model.User;
import com.usyd.elec5619.group14.repository.UserRepository;
import com.usyd.elec5619.group14.service.ApplicationService;
import com.usyd.elec5619.group14.service.JobPostService;
import com.usyd.elec5619.group14.service.JobSeekerService;
import com.usyd.elec5619.group14.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/jobportal/application")
public class ApplicationController {

    @Autowired
    ApplicationService applicationService;
    @Autowired
    JobSeekerService jobSeekerService;
    @Autowired
    JobPostService jobPostService;

    @RequestMapping("")
	public String getApplications(Model model, HttpSession session) {
    	if(session.getAttribute("userIdSession") == null) {
			return "redirect:/jobportal";
		}
        List<Application> applications = this.applicationService.listApplications();

        if(session.getAttribute("userRole").equals("USER_EMPLOYER")) {
            applications.removeIf(x -> x.getJobPost().getEmployer().getUserId() != (long)session.getAttribute("userIdSession"));
        } else {
            applications.removeIf(x -> x.getJobSeeker().getUserId() != (long)session.getAttribute("userIdSession"));
        }

		model.addAttribute("application", new Application());
		model.addAttribute("applications", applications);
	
		return "application";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateApplication(@ModelAttribute Application application, Model model, HttpSession session) {

        // JobSeeker jobSeeker = jobSeekerService.getJobSeekerById((long)session.getAttribute("userIdSession")).get();
        // Application application = new Application();
        // application.setJobPost(jobPost);
        // application.setJobSeeker(jobSeeker);
        // application.setApplicationStatus(false);
        
        application.setApplicationStatus(true);
        
        
		this.applicationService.addApplication(application);
		return "redirect:/dashboard";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addApplication(@ModelAttribute Application application, HttpSession session) {

        
        JobSeeker jobSeeker = this.jobSeekerService.getJobSeekerById((long) session.getAttribute("userIdSession")).get();

        //application.setJobPost(jobPost);
        application.setApplicationStatus(false);
        application.setJobSeeker(jobSeeker);
        
        
		this.applicationService.addApplication(application);
		return "redirect:/dashboard";
    }

    @RequestMapping(value = "/view/{id}")
	public String viewApplication(@PathVariable long id, Model model) {
		//model.addAttribute("comment", new Comment());
		//model.addAttribute("jobPost", this.jobPostService.getJobPost(id));
        //model.addAttribute("comments", this.commentService.getCommentsByJobPost(id));
        model.addAttribute("application", this.applicationService.getApplicationById(id));

		return "viewapplication";
	}
    
    // @RequestMapping(value = "/edit/{id}")
	// public String viewJobPost(@PathVariable long id, Model model) {
	// 	model.addAttribute("appl", this.jobPostService.getJobPost(id));
	// 	//model.addAttribute("jobPostApplicationDeadline", this.jobPostService.getJobPost(id).get().getJobPostApplicationDeadline().toString());
		
	// 	return "jobpostform";
	// }

}