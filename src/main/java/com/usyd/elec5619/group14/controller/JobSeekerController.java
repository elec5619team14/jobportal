package com.usyd.elec5619.group14.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.usyd.elec5619.group14.model.JobSeeker;
import com.usyd.elec5619.group14.model.User;
import com.usyd.elec5619.group14.service.JobSeekerService;
import com.usyd.elec5619.group14.service.UserService;


@Controller
@RequestMapping("/jobportal/jobseeker")
public class JobSeekerController {

/**	
	 * 显示用户的个人中心 
	 * @param request
	 * @param response
	 * @throws ServletException 
	 * @throws IOException
	 * @author Harry
	 */
	
	private JobSeekerService jobSeekerService;
	private UserService userService;
	
	@SuppressWarnings("unused")
	private void showPersonal(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException{
		//找到保存到session中的登录用户
		User user =  (User) request.getSession().getAttribute("user");
		//通过用户id重新查找用户
		User userNow = userService.findUserById(user.getUserId());
		
		//加载用户所属部门和角色，解决懒加载的问题
		Hibernate.initialize(userNow.getAddress());
		Hibernate.initialize(userNow.getEmail());
		Hibernate.initialize(userNow.getFirstName());
		Hibernate.initialize(userNow.getLastName());
		Hibernate.initialize(userNow.getPhoneNumber());
	
	
		//保存到request中，转发显示
		request.setAttribute("userNow", userNow);
		request.getRequestDispatcher("/jobportal/jobseeker/jobseeker.html").forward(request, response);//显示当前用户的个人信息
		
	}
	
	@RequestMapping("/add")
	public String JobSeekerForm(Model model) {
		model.addAttribute("jobSeeker", new JobSeeker());
		return "jobseekerform";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addJobSeeker(@ModelAttribute JobSeeker jobSeeker, Model model) {
		this.jobSeekerService.addJobSeeker(jobSeeker);;
		return "redirect:/jobseeker";
	}

	@RequestMapping(value = "/view/{userId}")
	public String viewEmployerForm(@PathVariable long id, Model model) {
		model.addAttribute("jobSeeker", this.jobSeekerService.getJobSeekerById(id));
		return "jobseeker";
	}

	@RequestMapping(value = "/edit/{userId}")
	public String editJobSeeker(@PathVariable long id, Model model) {
		model.addAttribute("jobSeeker", this.jobSeekerService.getJobSeekerById(id));
		return "jobseekerform";
	}

	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	public String updateJobSeeker(@ModelAttribute JobSeeker jobSeeker, Model model) {
		jobSeekerService.addJobSeeker(jobSeeker);;
		return "redirect:/jobseeker";
	}

	@RequestMapping("/delete/{userId}")
	public String deleteJobSeeker(@PathVariable long id) {
		jobSeekerService.removeJobSeeker(id);;
		return "redirect:/jobseeker";
	}
}
