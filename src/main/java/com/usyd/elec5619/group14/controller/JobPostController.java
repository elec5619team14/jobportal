package com.usyd.elec5619.group14.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.usyd.elec5619.group14.model.Comment;
import com.usyd.elec5619.group14.model.Employer;
import com.usyd.elec5619.group14.model.JobPost;
import com.usyd.elec5619.group14.service.CommentService;
import com.usyd.elec5619.group14.service.EmployerService;
import com.usyd.elec5619.group14.service.JobPostService;

@Controller
@RequestMapping("/jobportal/jobpost")
public class JobPostController {

	@Autowired
	private JobPostService jobPostService;
	@Autowired
	private EmployerService employerService;
	@Autowired
	private CommentService commentService;

	@RequestMapping("")
	public String getJobPost(Model model, HttpSession session) {
		if(session.getAttribute("userIdSession") == null) {
			return "redirect:/jobportal";
		}
		model.addAttribute("jobPost", new JobPost());

		List<JobPost> posts = this.jobPostService.listJobPosts();

		if(session.getAttribute("userRole").equals("USER_EMPLOYER")) {
			posts.removeIf(x -> (x.getEmployer().getUserId() != (long) session.getAttribute("userIdSession")));
		}
		
		model.addAttribute("jobPosts", posts);
		return "jobpost";
	}

	@RequestMapping("/add")
	public String jobPostForm(Model model) {
		model.addAttribute("jobPost", new JobPost());
		//model.addAttribute("jobPostApplicationDeadline", null);
		return "jobpostform";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addJobPost(@ModelAttribute JobPost jobPost, Model model, HttpSession session) {		
		Employer employer = employerService.getEmployerById((long) session.getAttribute("userIdSession")).get();
		jobPost.setEmployer(employer);
		this.jobPostService.addJobPost(jobPost);
		return "redirect:/jobportal/jobpost";
	}

	@RequestMapping(value = "/view/{id}")
	public String editJobPostForm(@PathVariable long id, Model model) {
		model.addAttribute("comment", new Comment());
		model.addAttribute("jobPost", this.jobPostService.getJobPost(id));
		model.addAttribute("comments", this.commentService.getCommentsByJobPost(id));
		return "viewjobpost";
	}

	@RequestMapping(value = "/edit/{id}")
	public String viewJobPost(@PathVariable long id, Model model) {
		model.addAttribute("jobPost", this.jobPostService.getJobPost(id));
		//model.addAttribute("jobPostApplicationDeadline", this.jobPostService.getJobPost(id).get().getJobPostApplicationDeadline().toString());
		
		return "jobpostform";
	}

	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	public String updateJobPost(@ModelAttribute JobPost jobPost, Model model) {
		jobPostService.addJobPost(jobPost);
		return "redirect:/jobportal/jobpost";
	}

	@RequestMapping("/delete/{id}")
	public String deleteJobPost(@PathVariable long id) {
		jobPostService.removeJobPost(id);
		return "redirect:/jobportal/jobpost";
	}
}
