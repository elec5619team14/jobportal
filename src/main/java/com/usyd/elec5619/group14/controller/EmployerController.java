package com.usyd.elec5619.group14.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.usyd.elec5619.group14.model.Employer;
import com.usyd.elec5619.group14.service.EmployerService;

@Controller
@RequestMapping("/jobportal/employer")
public class EmployerController {

	@Autowired
	private EmployerService employerService;

	@RequestMapping("")
	public String getEmployer(Model model) {
		model.addAttribute("employer", new Employer());
		model.addAttribute("employers", this.employerService.listEmployers());
	
		return "employer";
	}

	@RequestMapping("/add")
	public String employerForm(Model model) {
		model.addAttribute("employer", new Employer());
		return "employerform";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addEmployer(@ModelAttribute Employer employer, Model model) {
		this.employerService.addEmployer(employer);
		return "redirect:/employer";
	}

	@RequestMapping(value = "/view/{id}")
	public String viewEmployerForm(@PathVariable long id, Model model) {
		model.addAttribute("jobPost", this.employerService.getEmployerById(id));
		return "viewemployer";
	}

	@RequestMapping(value = "/edit/{id}")
	public String editEmployer(@PathVariable long id, Model model) {
		model.addAttribute("employer", this.employerService.getEmployerById(id));
		return "employerform";
	}

	@RequestMapping(value = "/add", method = RequestMethod.PUT)
	public String updateEmployer(@ModelAttribute Employer employer, Model model) {
		employerService.addEmployer(employer);
		return "redirect:/employer";
	}

	@RequestMapping("/delete/{id}")
	public String deleteEmployer(@PathVariable long id) {
		employerService.removeEmployer(id);
		return "redirect:/employer";
	}
}
