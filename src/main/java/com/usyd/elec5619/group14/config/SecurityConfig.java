package com.usyd.elec5619.group14.config;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

import com.usyd.elec5619.group14.service.UserService;

@Component
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService userService;

    //拦截请求
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers(
                            "src/main/resources/**",
                            "/jobportal/registration",
                            "/js/**",
                            "/demo/**",
                           "/scss/**",
                            "/css/**",
                            "/img/**",
                            "/webjars/**"
                            ).permitAll()
        //我们指定任何用户都可以访问多个URL的模式。
        //任何用户都可以访问以"/resources/","/registration", 或者 "/about"开头的URL。                                                     
       .antMatchers("/resources/**", "/jobportal/registration", "/jobportal/aboutus","/jobportal/dashboard","/jobportal/contactus").permitAll()     

        //以 "/employer/" 开头的URL只能让拥有 "employer"角色的用户访问。 /jobseeker 只能让jobseeker角色访问
        //使用 hasRole 方法，没有使用 "ROLE_" 前缀。               
//       .antMatchers("/jobportal/employer/**").hasRole("USER_EMPLOYER")  
//       .antMatchers("/jobportal/jobseeker/**").hasRole("USER_JOBSEEKER")     

        //尚未匹配的任何URL都要求用户进行身份验证

                    .anyRequest().authenticated()     //需要登录后才可以   anonymous是匿名也可访问
                .and()
                    .formLogin()
                        .loginPage("/jobportal/login")
                            .defaultSuccessUrl("/jobportal/dashboard")  //成功跳转dashboard
                            	.permitAll()
                .and()
                    .logout()
                        .invalidateHttpSession(true)
                        .clearAuthentication(true)
                        .logoutRequestMatcher(new AntPathRequestMatcher("/jobportal/logout"))
                        .logoutSuccessUrl("/jobportal/login?logout")
                .permitAll()
                .and()
                	.csrf().disable();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
        auth.setUserDetailsService(userService);
        auth.setPasswordEncoder(passwordEncoder());
        return auth;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }


}
