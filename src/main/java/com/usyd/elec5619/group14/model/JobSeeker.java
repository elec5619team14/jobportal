package com.usyd.elec5619.group14.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.usyd.elec5619.group14.model.constants.JobType;

@Entity
public class JobSeeker extends User {
	@Column(nullable = true)
	private String skills;
	@Column(nullable = true)
	private String qualification;
	@Column(nullable = true)
	private String workExperience;
	@Column(nullable = true)
	private String birthDate;
	@Column(nullable = true)
	private JobType jobType;
//	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@Column(nullable = true)
//	private Set<Application> applications = new HashSet<Application>(0);

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getWorkExperience() {
		return workExperience;
	}

	public void setWorkExperience(String workExperience) {
		this.workExperience = workExperience;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		this.birthDate = LocalDate.parse(birthDate, formatter).toString();
	}

	public JobType getJobType() {
		return jobType;
	}

	public void setJobType(JobType jobType) {
		this.jobType = jobType;
	}
	
//	public Set<Application> getJobPosts() {
//		return applications;
//	}
//
//	public void setJobPosts(Set<Application> applications) {
//		this.applications = applications;
//	}
	
}
