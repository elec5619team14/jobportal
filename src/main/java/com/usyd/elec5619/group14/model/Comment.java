package com.usyd.elec5619.group14.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Comment {
	@Id
	@GeneratedValue
	private long commentId;
	private String commentBody;
	@Column(nullable = true)
	private long parentId;
/*	@ManyToOne(fetch = FetchType.LAZY)
 *  @PrimaryKeyJoinColumn
	private User user;
*/	@ManyToOne(fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private JobPost jobPost;
	@ManyToOne(fetch = FetchType.EAGER)
	private User user;
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setCommentTime(LocalDateTime commentTime) {
		this.commentTime = commentTime;
	}

	private LocalDateTime commentTime;
	
	public long getCommentId() {
		return commentId;
	}
	
	public void setCommentId(long commentId) {
		this.commentId = commentId;
	}
	
	public String getCommentBody() {
		return commentBody;
	}
	
	public void setCommentBody(String commentBody) {
		this.commentBody = commentBody;
	}
	
	public long getParentId() {
		return parentId;
	}
	
	public void setParentId(long parentId) {
		this.parentId = parentId;
	}
	
/*	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
*/		
	public JobPost getJobPost() {
		return jobPost;
	}
	
	public void setJobPost(JobPost jobPost) {
		this.jobPost = jobPost;
	}
	
	public LocalDateTime getCommentTime() {
		return commentTime;
	}
	
	public void setCommentTime() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy'T'HH:mm");
		this.commentTime = LocalDateTime.parse(LocalDateTime.now().format(formatter), formatter);
	}
	
}
