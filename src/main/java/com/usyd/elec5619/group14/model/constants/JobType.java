package com.usyd.elec5619.group14.model.constants;

public enum JobType {
	FULL_TIME("Full Time"),
	PART_TIME("Part Time"),
	CASUAL("Casual");

	private final String displayName;

	JobType(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return this.displayName;
	}
}
