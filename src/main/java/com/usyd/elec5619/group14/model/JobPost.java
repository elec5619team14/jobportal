package com.usyd.elec5619.group14.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.usyd.elec5619.group14.model.constants.Industry;
import com.usyd.elec5619.group14.model.constants.JobType;

@Entity
public class JobPost {
	@Id
	@GeneratedValue
	private long jobPostId;	
	private String jobPostTitle;	
	private String jobPostDesc;	
	private JobType jobPostType;	
	private Industry jobPostIndustry;	
	private String jobPostPosition;
	private String jobPostLocation;
	private double jobPostSalary;
	private String jobPostApplicationDeadline;
	private String jobPostPostedTime;
//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@Column(nullable = true)
//	private Set<Comment> comments = new HashSet<Comment>(0);
	@ManyToOne(fetch = FetchType.EAGER)
	private Employer employer;
//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@Column(nullable = true)
//	private Set<Application> applications = new HashSet<Application>(0);

	public JobPost() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
		this.jobPostPostedTime = LocalDateTime.parse(LocalDateTime.now().format(formatter), formatter).toString();		
	}

	public JobType getJobPostType() {
		return jobPostType;
	}

	public void setJobPostType(JobType jobPostType) {
		this.jobPostType = jobPostType;
	}

	public Industry getJobPostIndustry() {
		return jobPostIndustry;
	}

	public void setJobPostIndustry(Industry jobPostIndustry) {
		this.jobPostIndustry = jobPostIndustry;
	}

	public String getJobPostPosition() {
		return jobPostPosition;
	}

	public void setJobPostPosition(String jobPostPosition) {
		this.jobPostPosition = jobPostPosition;
	}

	public String getJobPostLocation() {
		return jobPostLocation;
	}

	public void setJobPostLocation(String jobPostLocation) {
		this.jobPostLocation = jobPostLocation;
	}

	public double getJobPostSalary() {
		return jobPostSalary;
	}

	public void setJobPostSalary(double jobPostSalary) {
		this.jobPostSalary = jobPostSalary;
	}

	public long getJobPostId() {
		return jobPostId;
	}

	public void setJobPostId(long jobPostId) {
		this.jobPostId = jobPostId;
	}

	public String getJobPostTitle() {
		return jobPostTitle;
	}

	public void setJobPostTitle(String jobPostTitle) {
		this.jobPostTitle = jobPostTitle;
	}

	public String getJobPostDesc() {
		return jobPostDesc;
	}

	public void setJobPostDesc(String jobPostDesc) {
		this.jobPostDesc = jobPostDesc;
	}

	public String getJobPostApplicationDeadline() {
		return jobPostApplicationDeadline;
	}

	public void setJobPostApplicationDeadline(String localDateTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
		this.jobPostApplicationDeadline = LocalDateTime.parse(localDateTime, formatter).toString();
	}

	public String getJobPostPostedTime() {
		return jobPostPostedTime.toString();
	}

	public void setJobPostPostedTime() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
		this.jobPostPostedTime = LocalDateTime.parse(LocalDateTime.now().format(formatter), formatter).toString();		
	}
	
//	public Set<Comment> getComments() {
//		return comments;
//	}
//
//	public void setComments(Set<Comment> comments) {
//		this.comments = comments;
//	}	
	
	public Employer getEmployer() {
		return employer;
	}
	
	public void setEmployer(Employer employer) {
		this.employer = employer;
	}

//	public Set<Application> getApplications() {
//		return applications;
//	}
//
//	public void setApplications(Set<Application> applications) {
//		this.applications = applications;
//	}
}
