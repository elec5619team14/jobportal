package com.usyd.elec5619.group14.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class Application {
	@Id
	@GeneratedValue
	private long applicationId;
	@ManyToOne(fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private JobPost jobPost;
	@ManyToOne(fetch = FetchType.EAGER)
	@PrimaryKeyJoinColumn
	private JobSeeker jobSeeker;
	private boolean applicationStatus;
	private String applicationDate;

	public Application() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
		this.applicationDate = LocalDateTime.parse(LocalDateTime.now().format(formatter), formatter).toString();
	}
	
	public long getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(long applicationId) {
		this.applicationId = applicationId;
	}
	public JobPost getJobPost() {
		return jobPost;
	}
	public void setJobPost(JobPost jobPost) {
		this.jobPost = jobPost;
	}
	public JobSeeker getJobSeeker() {
		return jobSeeker;
	}
	public void setJobSeeker(JobSeeker jobSeeker) {
		this.jobSeeker = jobSeeker;
	}
	public boolean getApplicationStatus() {
		return applicationStatus;
	}
	public void setApplicationStatus(boolean applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	public String getApplicationDate() {
		return applicationDate;
	}
	public void setApplicationDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
		this.applicationDate = LocalDateTime.parse(LocalDateTime.now().format(formatter), formatter).toString();
	}
}
