package com.usyd.elec5619.group14.model.dto;

import com.usyd.elec5619.group14.model.constants.Industry;
import com.usyd.elec5619.group14.model.constants.JobType;

public class JobPostDto {
    private String title;
    private String description;
    private JobType type;
    private Industry industry;
    private String position;
    private String location;
    private double salary;
    private String deadline;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public JobType getType() {
        return this.type;
    }

    public void setType(JobType type) {
        this.type = type;
    }

    public Industry getIndustry() {
        return this.industry;
    }

    public void setIndustry(Industry industry) {
        this.industry = industry;
    }

    public String getPosition() {
        return this.position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getSalary() {
        return this.salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getDeadline() {
        return this.deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }
    

}