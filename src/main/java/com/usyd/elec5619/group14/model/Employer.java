package com.usyd.elec5619.group14.model;

import javax.persistence.Entity;

import com.usyd.elec5619.group14.model.constants.Industry;

@Entity
public class Employer extends User {
	private String companyName;
	private String companyDescription;
	private Industry industry;
//	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@Column(nullable = true)
//	private Set<JobPost> jobPosts = new HashSet<JobPost>();

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyDescription() {
		return companyDescription;
	}

	public void setCompanyDescription(String companyDescription) {
		this.companyDescription = companyDescription;
	}

	public Industry getIndustry() {
		return industry;
	}

	public void setIndustry(Industry industry) {
		this.industry = industry;
	}
	
//	public Set<JobPost> getJobPosts() {
//		return jobPosts;
//	}
//
//	public void setJobPosts(Set<JobPost> jobPosts) {
//		this.jobPosts = jobPosts;
//	}
	
}
