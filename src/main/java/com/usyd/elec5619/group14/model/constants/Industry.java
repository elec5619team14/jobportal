package com.usyd.elec5619.group14.model.constants;

public enum Industry {
	IT("IT"),
	ENGINEERING("Engineering"),
	DATA_ANALYTICS("Data Analytics"),
	SECURITY("Security"),
	BUSINESS("Business"),
	ACCOUNTING("Accounting"),
	FINANCE("Finance"),
	MARKETING("Marketing"),
	MANAGEMENT("Management"),
	DELIVERY("Delivery"),
	POSTAL("Postal"),
	SUPPLY_CHAIN("Supply Chain"),
	AGRICULTURE("Agriculture"),
	HOSPITALITY("Hospitality"),
	BANKING("Banking"),
	FAST_FOOD("Fast Food"),
	RESTAURANT("Restaurant");

	private final String displayName;

	Industry(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return this.displayName;
	}
}
