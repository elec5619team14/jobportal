package com.usyd.elec5619.group14.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usyd.elec5619.group14.model.Employer;

import com.usyd.elec5619.group14.repository.EmployerRepository;


@Service
public class EmployerServiceImpl implements EmployerService {

	@Autowired
	private EmployerRepository employerRepository;

	@Override
	public void addEmployer(Employer p) {
		this.employerRepository.save(p);
	}

	@Override
	public void updateEmployer(Employer p) {
		this.employerRepository.save(p);
	}

	@Override
	public List<Employer> listEmployers() {
		return (List<Employer>) this.employerRepository.findAll();
	}

	@Override
	public Optional<Employer> getEmployerById(long id) {
		return this.employerRepository.findById(id);
	}

	@Override
	public void removeEmployer(long id) {
		this.employerRepository.deleteById(id);
	}
	@Override
	public Employer findOne() {
		return this.employerRepository.findAll().iterator().next();
	}

//	@EventListener
//	public void seed(ContextRefreshedEvent event) {
//		for (int i = 0; i < 2; i++) {
//			Employer employer = new Employer();
//			employer.setAddress("Sydney" + i);
//			employer.setCompanyDescription("IT Service Management Company" + i);
//			employer.setCompanyName("PwC-Price Water Cooper" + i);
//			employer.setEmail("info@pwc.com" + i);
//			employer.setIndustry(Industry.IT);
//			employer.setPassword("testpassword" + i);
//			employer.setPhoneNumber("+4523788976" + i);
////			employer.setUserName("pwcadmin" + i);
////			employer.setRole(Role.EMPLOYER);
//			employerRepository.save(employer);
//		}
//	}
}
