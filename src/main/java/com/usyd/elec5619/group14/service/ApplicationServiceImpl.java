package com.usyd.elec5619.group14.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.usyd.elec5619.group14.model.Application;
import com.usyd.elec5619.group14.repository.ApplicationRepository;

@Service
public class ApplicationServiceImpl implements ApplicationService {
	
	private ApplicationRepository applicationRepository;
	
	@Autowired
	public void setApplicationRepository(ApplicationRepository applicationRepository) {
		this.applicationRepository = applicationRepository;
	}

	@Override
	@Transactional
	public void addApplication(Application p) {
		this.applicationRepository.save(p);
	}

	@Override
	@Transactional
	public void updateApplication(Application p) {
		this.applicationRepository.save(p);
	}

	@Override
	@Transactional
	public List<Application> listApplications() {
		return (List<Application>) this.applicationRepository.findAll();
	}

	@Override
	@Transactional
	public Optional<Application> getApplicationById(long id) {
		return this.applicationRepository.findById(id);
	}

	@Override
	@Transactional
	public void removeApplication(long id) {
		this.applicationRepository.deleteById(id);
	}

}
