package com.usyd.elec5619.group14.service;

import java.util.List;
import java.util.Optional;

import com.usyd.elec5619.group14.model.Application;


public interface ApplicationService {

	public void addApplication(Application p);
	public void updateApplication(Application p);
	public List<Application> listApplications();
	public Optional<Application> getApplicationById(long id);
	public void removeApplication(long id);
	
}
