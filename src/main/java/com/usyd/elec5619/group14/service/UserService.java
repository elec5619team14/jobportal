package com.usyd.elec5619.group14.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.usyd.elec5619.group14.model.User;
import com.usyd.elec5619.group14.model.dto.UserDto;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(UserDto userDto);
    
    User findUserById(long userId);


}
