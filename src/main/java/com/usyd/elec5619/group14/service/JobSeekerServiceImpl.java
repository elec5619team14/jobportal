package com.usyd.elec5619.group14.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.usyd.elec5619.group14.model.JobSeeker;
import com.usyd.elec5619.group14.repository.JobSeekerRepository;

@Service
public class JobSeekerServiceImpl implements JobSeekerService {
	
	@Autowired
	private JobSeekerRepository jobSeekerRepository;
	

	@Override
	public void addJobSeeker(JobSeeker js) {
		this.jobSeekerRepository.save(js);
	}

	@Override
	public void updateJobSeeker(JobSeeker js) {
		this.jobSeekerRepository.save(js);
	}

	@Override
	public List<JobSeeker> listJobSeekers() {
		return (List<JobSeeker>) this.jobSeekerRepository.findAll();
	}

	@Override
	public Optional<JobSeeker> getJobSeekerById(long id) {
		return this.jobSeekerRepository.findById(id);
	}

	@Override
	public void removeJobSeeker(long id) {
		this.jobSeekerRepository.deleteById(id);
	}

	
}
