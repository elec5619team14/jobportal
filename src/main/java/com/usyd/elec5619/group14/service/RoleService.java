package com.usyd.elec5619.group14.service;

import java.util.Optional;

import com.usyd.elec5619.group14.model.Role;

public interface RoleService {
	public Optional<Role> getRoleById(long id);
}
