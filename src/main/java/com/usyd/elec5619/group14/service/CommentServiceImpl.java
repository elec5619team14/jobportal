package com.usyd.elec5619.group14.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usyd.elec5619.group14.model.Comment;
import com.usyd.elec5619.group14.repository.CommentRepository;


@Service
public class CommentServiceImpl implements CommentService {
	
	@Autowired
	private CommentRepository commentRepository;
	
	@Override
	public void addComment(Comment p) {
		this.commentRepository.save(p);
	}

	@Override
	public void updateComment(Comment p) {
		this.commentRepository.save(p);
	}

	@Override
	public List<Comment> listComments() {
		return (List<Comment>) this.commentRepository.findAll();
	}

	@Override
	public Comment getCommentById(long id) {
		return this.commentRepository.getOne(id);
	}

	@Override
	public void removeComment(long id) {
		this.commentRepository.deleteById(id);
	}

	@Override
	public List<Comment> getCommentsByJobPost(long id) {
		return this.commentRepository.findByJobPostJobPostId(id);
	}

}
