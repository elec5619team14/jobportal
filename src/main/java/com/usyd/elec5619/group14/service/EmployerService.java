package com.usyd.elec5619.group14.service;

import java.util.List;
import java.util.Optional;

import com.usyd.elec5619.group14.model.Employer;


public interface EmployerService {

	public void addEmployer(Employer p);
	public void updateEmployer(Employer p);
	public List<Employer> listEmployers();
	public Optional<Employer> getEmployerById(long id);
	public void removeEmployer(long id);
	public Employer findOne();
}
