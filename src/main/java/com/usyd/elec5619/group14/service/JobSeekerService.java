package com.usyd.elec5619.group14.service;

import java.util.List;
import java.util.Optional;

import com.usyd.elec5619.group14.model.JobSeeker;

public interface JobSeekerService {

	public void addJobSeeker(JobSeeker js);
	public void updateJobSeeker(JobSeeker js);
	
	public List<JobSeeker> listJobSeekers();
	public Optional<JobSeeker> getJobSeekerById(long id);
	public void removeJobSeeker(long id);
	
}
