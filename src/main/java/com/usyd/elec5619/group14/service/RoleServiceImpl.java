package com.usyd.elec5619.group14.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.usyd.elec5619.group14.model.Role;
import com.usyd.elec5619.group14.repository.RoleRepository;

public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public Optional<Role> getRoleById(long id) {
		return roleRepository.findById(id);
	}

}
