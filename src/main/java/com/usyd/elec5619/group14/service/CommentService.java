package com.usyd.elec5619.group14.service;

import java.util.List;
import java.util.Optional;

import com.usyd.elec5619.group14.model.Comment;


public interface CommentService {

	public void addComment(Comment p);
	public void updateComment(Comment p);
	public List<Comment> listComments();
	public Comment getCommentById(long id);
	public void removeComment(long id);
	public List<Comment> getCommentsByJobPost(long id);
}
