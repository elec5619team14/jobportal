package com.usyd.elec5619.group14.restapi;

import java.util.List;
import java.util.Optional;

import com.usyd.elec5619.group14.model.JobPost;


public interface JobPostServiceAPI {

	public void addJobPost(JobPost p);
	public void updateJobPost(JobPost p);
	public List<JobPost> listJobPosts();
	public Optional<JobPost> getJobPost(long id);
	public void removeJobPost(long id);
}

