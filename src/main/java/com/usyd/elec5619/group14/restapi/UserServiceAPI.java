package com.usyd.elec5619.group14.restapi;

import java.util.List;
import java.util.Optional;

import com.usyd.elec5619.group14.model.User;

public interface UserServiceAPI {
	public void addUser(User p);
	public void updateUser(User p);
	public List<User> listUsers();
	public Optional<User> getUserById(long id);
	public void removeUser(long id);
	public User findOne();
}