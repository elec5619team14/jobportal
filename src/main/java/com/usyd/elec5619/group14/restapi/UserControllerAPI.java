package com.usyd.elec5619.group14.restapi;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.usyd.elec5619.group14.model.User;

@RestController
public class UserControllerAPI {
	
	@Autowired
	private UserServiceAPI userServiceAPI;
	
	//working
	@RequestMapping("/api/users")
	public List<User> getAllUsers() {
		return userServiceAPI.listUsers();
	}
	
	//working
	@RequestMapping(method=RequestMethod.GET, value="/api/users/{id}")
	public Optional<User> getUserById(@PathVariable long id) {
		return userServiceAPI.getUserById(id);
	}
	
	//working
	@RequestMapping(method=RequestMethod.POST, value="/api/users")
	public void addUser(@RequestBody User user) {
		userServiceAPI.addUser(user);
	}	
	
	//working
	@RequestMapping(method=RequestMethod.DELETE, value="/api/users/delete/{id}")
	public void deleteUser(@PathVariable long id) {
		userServiceAPI.removeUser(id);
	}
	
	//working
	@RequestMapping(method=RequestMethod.PUT, value="/api/users/update/{id}")
	public void updateUser(@RequestBody User user, @PathVariable long id) {
		userServiceAPI.updateUser(user);
	}	
}
