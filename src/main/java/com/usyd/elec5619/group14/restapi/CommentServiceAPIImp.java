package com.usyd.elec5619.group14.restapi;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usyd.elec5619.group14.model.Comment;
import com.usyd.elec5619.group14.repository.CommentRepository;


@Service
public class CommentServiceAPIImp implements CommentServiceAPI {
	
	@Autowired
	private CommentRepository commentRepository;
	
	//working
	@Override
	public List<Comment> listComments() {
		return (List<Comment>) this.commentRepository.findAll();
	}
		
	@Override
	public Optional<Comment> getCommentById(long id) {
		return this.commentRepository.findById(id);
	}	
	
	@Override
	public void addComment(Comment p) {
		this.commentRepository.save(p);
	}

	@Override
	public void updateComment(Comment p) {
		this.commentRepository.save(p);
	}

	@Override
	public void removeComment(long id) {
		this.commentRepository.deleteById(id);
	}
}
