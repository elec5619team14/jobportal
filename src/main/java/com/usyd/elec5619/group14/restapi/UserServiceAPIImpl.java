package com.usyd.elec5619.group14.restapi;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usyd.elec5619.group14.model.User;
import com.usyd.elec5619.group14.repository.UserRepository;

@Service
public class UserServiceAPIImpl implements UserServiceAPI {
	@Autowired
	private UserRepository userRepository;

	//working
	@Override
	public List<User> listUsers() {
		return (List<User>) this.userRepository.findAll();
	}
	
	//working
	@Override
	public Optional<User> getUserById(long id) {
		return this.userRepository.findById(id);
	}	
	
	//working
	@Override
	public void addUser(User p) {
		this.userRepository.save(p);
	}
	
	//working
	@Override
	public void removeUser(long id) {
		this.userRepository.deleteById(id);		
	}

	
	//working
	@Override
	public void updateUser(User p) {
		this.userRepository.save(p);
	}

	@Override
	public User findOne() {
		return this.userRepository.findAll().iterator().next();
	}
}
