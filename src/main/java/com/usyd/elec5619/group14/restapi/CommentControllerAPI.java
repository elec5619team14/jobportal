package com.usyd.elec5619.group14.restapi;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.usyd.elec5619.group14.model.Comment;

@RestController
public class CommentControllerAPI {
	
	@Autowired
	private CommentServiceAPI commentServiceAPI;
	
	//working
	@RequestMapping(method=RequestMethod.GET, value="/api/comments")
	public List<Comment> getAllComments() {
		return commentServiceAPI.listComments();
	}
	
	//working
	@RequestMapping(method = RequestMethod.GET, value="/api/comments/{id}")
	public Optional<Comment> getCommentById(@PathVariable long id) {
		return commentServiceAPI.getCommentById(id);
	}
	
	//working
	@RequestMapping(method=RequestMethod.POST, value="/api/comments/add")
	public void addComment(@RequestBody Comment comment) {
		commentServiceAPI.addComment(comment);
	}
	
	//working
	@RequestMapping(method = RequestMethod.PUT, value = "/api/comments/update")
	public void updateComment(@RequestBody Comment comment) {
		commentServiceAPI.updateComment(comment);
	}
	
	//working
	@RequestMapping(method = RequestMethod.DELETE, value="/api/comments/delete/{id}")
	public void deleteJobPost(@PathVariable long id) {
		commentServiceAPI.removeComment(id);
	}	
}
