package com.usyd.elec5619.group14.restapi;

import java.util.List;
import java.util.Optional;

import com.usyd.elec5619.group14.model.Comment;


public interface CommentServiceAPI {

	public void addComment(Comment p);
	public void updateComment(Comment p);
	public List<Comment> listComments();
	public Optional<Comment> getCommentById(long id);
	public void removeComment(long id);
//	public List<Comment> getCommentsByJobPost(long id);
}