package com.usyd.elec5619.group14.restapi;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usyd.elec5619.group14.model.JobPost;

import com.usyd.elec5619.group14.repository.JobPostRepository;

@Service
public class JobPostServiceAPIImpl implements JobPostServiceAPI {

	@Autowired
	private JobPostRepository jobPostRepository;

	//working
	@Override
	public Optional<JobPost> getJobPost(long id) {
		return this.jobPostRepository.findById(id);
	}
		
	//working
	@Override
	public List<JobPost> listJobPosts() {
		return (List<JobPost>) this.jobPostRepository.findAll();
	}	
	
	@Override
	public void addJobPost(JobPost p) {
		this.jobPostRepository.save(p);
	}

	@Override
	public void updateJobPost(JobPost p) {
		this.jobPostRepository.save(p);
	}
	@Override
	public void removeJobPost(long id) {
		this.jobPostRepository.deleteById(id);
	}
}
