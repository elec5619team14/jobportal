package com.usyd.elec5619.group14.restapi;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.usyd.elec5619.group14.model.JobPost;

@RestController
public class JobPostControllerAPI {

	@Autowired
	private JobPostServiceAPI jobPostServiceAPI;
	
	//working
	@RequestMapping("/api/jobpost")
	public List<JobPost> getAllJobPost() {
		return jobPostServiceAPI.listJobPosts();
	}
	
	//working
	@RequestMapping(method = RequestMethod.GET, value="/api/jobpost/{id}")
	public Optional<JobPost> getJobPostById(@PathVariable long id) {
		return jobPostServiceAPI.getJobPost(id);
	}
	
	//working
	@RequestMapping(method = RequestMethod.POST, value = "/api/jobpost/add")
	public void addJobPost(@RequestBody JobPost jobPost) {
		this.jobPostServiceAPI.addJobPost(jobPost);
	}

	//working
	@RequestMapping(method = RequestMethod.PUT, value = "/api/jobpost/update")
	public void updateJobPost(@RequestBody JobPost jobPost) {
		jobPostServiceAPI.addJobPost(jobPost);
	}

	//working
	@RequestMapping(method = RequestMethod.DELETE, value="/api/jobpost/delete/{id}")
	public void deleteJobPost(@PathVariable long id) {
		jobPostServiceAPI.removeJobPost(id);
	}
}

